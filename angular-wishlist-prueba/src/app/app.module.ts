import { BrowserModule } from '@angular/platform-browser';
import {
  NgModule,
  InjectionToken,
  APP_INITIALIZER,
  Injectable,
} from '@angular/core';
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
//routing
//import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { ItemsListarComponent } from './items-listar/items-listar.component';
import { RouterModule, Routes } from '@angular/router';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';

import {
  DestinosViajesState,
  reducerDestinosViajes,
  intializeDestinosViajesState,
  DestinosViajesEffects,
} from './models/destinos-viajes-state';
import { StoreDevtools, StoreDevtoolsModule } from '@ngrx/store-devtools';

//REDUX INIT
export interface AppState {
  destinos: DestinosViajesState;
  //aquí podriamor tener mensajeria o login o multiples cosas
}
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes,
};
let reducerInitialState = {
  destinos: intializeDestinosViajesState(),
};

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino', component: DestinoDetalleComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    ItemsListarComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducerInitialState }),
    EffectsModule.forRoot(DestinosViajesEffects),
    StoreDevtoolsModule
      .instrument
      //maxage permite ver la cantidad de cambios de estado
      (),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
