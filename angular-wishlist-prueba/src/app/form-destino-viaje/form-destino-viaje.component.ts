import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { fromEvent } from 'rxjs';
import { DestinoViaje } from '../models/destino.interface';
import {
  debounceTime,
  distinctUntilKeyChanged,
  filter,
  map,
  switchMap,
} from 'rxjs/operators';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css'],
})
export class FormDestinoViajeComponent implements OnInit {
  @Output()
  onItemAdded!: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongNombre = 3;
  searchResults: string[];

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: [
        '',
        Validators.compose([Validators.required, this.nombreValidator]),
      ],
      imagenUrl: [''],
    });
    this.fg.valueChanges.subscribe((form: any) => {
      console.log(form);
    });
  }

  ngOnInit() {
    //detectamos a medida el tipado ir levantando textot para buscar suguerencias
    let elementNombre = <HTMLInputElement>document.getElementById('nombre');
    //los pipers nos permiten hacer peticiones o busquedas de una manera más sencilla
    //ejemplo si quiero suguerir cuando el usuario deje de teclear y pasen unso segundos
    fromEvent(elementNombre, 'input')
      .pipe(
        //tra string de evento de teclado
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter((text) => text.length > 2), //nos intereza el valor y que sea mayor a 2
        //com opaso el filter entonces sigue
        debounceTime(200), //espere 2 decimas de sgundo
        distinctUntilKeyChanged(), //solo pasara si tiene nuevos caracteres
        switchMap(() => ajax('/assets/datos.json'))
        //si camio ahora si nos suscribimos
      )
      .subscribe((ajaxResponse) => {
        console.log(ajaxResponse);

        this.searchResults = ajaxResponse.response;
      });
  }

  guardar(nombre: string, imagenUrl: string): boolean {
    const d = new DestinoViaje(nombre, imagenUrl);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const longitud = control.value.toString().trim().length;
    if (longitud > 0 && longitud < 3) {
      return { invalidNombre: true };
    }
    return {};
  }
  /*nombreValidatorParametrizable(minLoing : number) : ValidatorFn {
    return (control : FromControl) : { [s: string]; boolean } | null => {
      if (longitud > 0 && longitud < 3) {
        return { minLongNombre: true };
      }
      return {};

    }
  }*/
}
function ajax(arg0: string): any {
  throw new Error('Function not implemented.');
}
