import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from '../models/destinos-api-client.modle';
import { DestinoViaje } from '../models/destino.interface';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
})
export class DestinoDetalleComponent implements OnInit {
  destino?: DestinoViaje;

  constructor(
    private route: ActivatedRoute,
    private destinoApiCliente: DestinosApiClient
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    //this.destino? = null!;
  }
}
