import {
  Component,
  OnInit,
  Input,
  HostBinding,
  EventEmitter,
  Output,
} from '@angular/core';

import { DestinoViaje } from '../models/destino.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
})
export class DestinoViajeComponent implements OnInit {
  //Ingresa como parametro
  //@Input() nombre?: String;
  @Input() destino?: DestinoViaje;
  @Input() position?: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  //salida
  @Output() clicked!: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    //la llamamos
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {}
  ir() {
    //llmaremos cliked com oevento y le diremos al componente padre el destino seleccionads
    this.clicked.emit(this.destino);
    return false;
  }
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino!));
    return false;
  }
  voteDown() {
    //enviar el no me gusta
    this.store.dispatch(new VoteDownAction(this.destino!));
    return false;
  }
}
