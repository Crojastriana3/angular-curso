import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-wishlist-prueba';

  //el rime va a observar el cambio del tiempo cada 1 segundo
  //con esto se hizo el reloj con esto podemos con cierto tiempo hacer consultas cada dicho tiempo.

  time = new Observable((observer) => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  });
}
