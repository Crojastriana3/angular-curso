/*import { DestinoViaje } from './destino.interface';
import { NuevoDestinoAction } from './destino-viajes-satate.model';
import { Subject, BehaviorSubject } from 'rxjs';

//redux
import { Injectable, Inject, forwardRef } from '@angular/core';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
  DestinosViajesState,
  NuevoDestinoAction,
  ElegidoFavoritoAction,
} from './destino-viajes-satate.model';
import {
  AppState,
  APP_CONFIG,
  AppConfig,
  db,
  MyDatabase,
} from './../app.module';
import {
  HttpRequest,
  HttpHeaders,
  HttpClient,
  HttpEvent,
  HttpResponse,
} from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];
  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
    this.store
      .select((state) => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    this.store.subscribe((data) => {
      console.log('all store');
      console.log(data);
    });
  }

  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({
      'X-API-TOKEN': 'token-seguridad',
    });
    const req = new HttpRequest(
      'POST',
      this.config.apiEndpoint + '/my',
      { nuevo: d.nombre },
      { headers: headers }
    );
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then((destinos) => console.log(destinos));
      }
    });
  }

  getById(id: String): DestinoViaje {
    return this.destinos.filter((d) => {
      return d.id.toString() == id;
    })[0];
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}*/
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppState } from '../app.module';
import { DestinoViaje } from './destino.interface';
import {
  ElegidoFavoritoAction,
  NuevoDestinoAction,
} from './destinos-viajes-state';

@Injectable()
export class DestinosApiClient {
  // destinos?: DestinoViaje[];
  //subject de la libreria rxjs
  //y me devuele un observable con multiples elementos
  // current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

  constructor(private store: Store<AppState>) {
    //this.destinos = [];
  }
  add(d: DestinoViaje) {
    this.store.dispatch(new NuevoDestinoAction(d));
    //this.destinos?.push(d);
  }
  /*getAlle(): DestinoViaje[] {
    return this.destinos!;
  }*/
  /*getById(id: String): any {
    return this.destinos?.filter(function (d) {
      return d.id.toString() === id;
    })[0];
  }*/
  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
    //permite propagar el evento
    /*this.destinos?.forEach((x) => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);*/
  }
  /*suscriberOnChage(fn: any) {
    this.current.subscribe(fn);
  }*/
}
