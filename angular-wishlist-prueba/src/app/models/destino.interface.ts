import { v4 as uuid } from 'uuid';

export class DestinoViaje {
  private selected!: boolean;
  public servicios!: string[];
  id = uuid();
  constructor(
    public nombre: string,
    public url: string,
    public votes: number = 0
  ) {
    this.servicios = ['Desayuno', 'Alejamiento'];
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected(x: boolean) {
    this.selected = true;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
