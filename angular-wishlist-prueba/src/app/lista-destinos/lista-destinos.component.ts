import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { DestinoViaje } from '../models/destino.interface';
import { DestinosApiClient } from '../models/destinos-api-client.modle';
import {
  ElegidoFavoritoAction,
  NuevoDestinoAction,
} from '../models/destinos-viajes-state';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
//import { DestinosApiClient } from '../models/destinos-api-client.modle';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  //providers: [DestinosApiClient],
})
export class ListaDestinosComponent implements OnInit {
  destinos: String[];
  destinosV?: DestinoViaje[];
  @Output() onItemAdded!: EventEmitter<DestinoViaje>;
  updates: string[];
  all: any;
  //public destinosApiClient: DestinosApiClient
  constructor(
    private destinosApiClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
    this.destinos = ['Barranquilla', 'Pitalito', 'Garzon', 'San agustin'];
    this.destinosV = [];
    this.updates = [];
    store
      .select((state) => state.destinos.favorito)
      .subscribe((data) => {
        const fav = data;
        if (data != null) {
          //nos montamos de manera reactiva sobre los cambios
          this.updates.push('Se ha elegido' + data.nombre);
        }
      });
    store
      .select((state) => state.destinos.items)
      .subscribe((items) => (this.all = items));
    //nos suscribimos a los cambios
    /*destinosApiClient.suscriberOnChage((d: DestinoViaje) => {
      if (d != null) {
        this.updates.push('Se ha elegido' + d.nombre);
      }
    });*/
  }

  ngOnInit(): void {}
  agregado(d: DestinoViaje) {
    //console.log(new DestinoViaje(nombre, url));
    //this.destinosV?.push(new DestinoViaje(nombre, url));
    console.log(this.destinosV);
    //this.destinosApiClient.add(d);
    this.destinosV?.push(d);
    this.onItemAdded.emit(d);
    //despachacmos la ascciones con redux
    this.store.dispatch(new NuevoDestinoAction(d));
  }
  elegido(d: DestinoViaje) {
    /*this.destinosV!.forEach(function (x) {
      x.setSelected(false);
    });
    d.setSelected(true);*/
    //le enviamos a la api esa infomación para que ella se encargue
    this.destinosApiClient.elegir(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  getAll() {}
}
