import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsListarComponent } from './items-listar.component';

describe('ItemsListarComponent', () => {
  let component: ItemsListarComponent;
  let fixture: ComponentFixture<ItemsListarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemsListarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
