import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-items-listar',
  templateUrl: './items-listar.component.html',
  styleUrls: ['./items-listar.component.css'],
})
export class ItemsListarComponent implements OnInit {
  nombres?: String[];
  constructor() {
    this.nombres = ['TETS', 'TEST1', 'TEST2', 'TEST3'];
  }

  ngOnInit(): void {}
}
